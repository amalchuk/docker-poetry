ARG PYTHON_VERSION
FROM python:${PYTHON_VERSION}-bullseye

ARG POETRY_VERSION
COPY ["docker-poetry.sh", "/usr/local/bin/"]

RUN ["docker-poetry.sh"]
CMD ["poetry"]
