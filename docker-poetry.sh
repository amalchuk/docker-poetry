#!/usr/bin/env bash

# Exit immediately if a pipeline returns a non-zero status:
set -eux

# Install Poetry to the $POETRY_HOME directory:
export POETRY_HOME="/poetry"

if [ ! -d "$POETRY_HOME" ]; then
    # Ensures that $POETRY_HOME exists or create it:
    mkdir $POETRY_HOME
fi

curl --request GET \
    --url https://install.python-poetry.org \
    --silent \
    --show-error \
    --output $POETRY_HOME/get-poetry.py

python $POETRY_HOME/get-poetry.py \
    --version $POETRY_VERSION \
    --yes

rm --force $POETRY_HOME/get-poetry.py
ln --symbolic --force $POETRY_HOME/bin/poetry /usr/local/bin/poetry

# Good-bye, cruel world!
unset POETRY_HOME
rm -- "$0"
